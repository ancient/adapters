var assert = require('chai').assert;
var Adapters = require('../index.js').Adapters;
var EventEmitter = require('../index.js').EventEmitter;

describe('ancient-adapters', function() {
  it('function as adapter', function() {
    var adapters = new Adapters();
    adapters.add('function', function() {
      this.method = function() { return 'function method'; };
    })
    var adapter = adapters.get('function');
    assert.isObject(adapter);
    assert.equal(adapter.method(), 'function method');
  });
  
  it('object as adapter', function() {
    var adapters = new Adapters();
    adapters.add('object', {
      method: function() { return 'object method'; }
    })
    var adapter = adapters.get('object');
    assert.isObject(adapter);
    assert.equal(adapter.method(), 'object method');
  });
  
  describe('EventEmitter', function() {
    it('errors and results', function() {
      var ee = new EventEmitter();
      var error = new Error('abc');
      ee.on('test', function(a) { if (a) return 123 });
      ee.on('test', function() {});
      ee.on('test', function(a) { if (a) throw error; });
      assert.lengthOf(ee.emit('test'), 0);
      assert.lengthOf(ee.emit('test', [true]), 2);
      assert.deepEqual(ee.emit('test', [true]), [123, error]);
    });
  });
  
  it('Adapters events', function() {
    var adapters = new Adapters();
    
    var willAdded = false;
    adapters.on('adapterWillAdded', function(name, adapter) {
      assert.equal(name, 'adaptersEvents');
      assert.isFunction(adapter.method);
      assert.equal(adapter.method(), 'events method');
      willAdded = true;
    });
    
    var didAdded = false;
    adapters.on('adapterDidAdded', function(name, adapter) {
      assert.equal(name, 'adaptersEvents');
      assert.isFunction(adapter.method);
      assert.equal(adapter.method(), 'events method');
      didAdded = true;
    });
    
    var notTriggered = true;
    var listener = function() {
      notTriggered = false;
    };
    adapters.on('adapterDidAdded', listener);
    adapters.off('adapterDidAdded', listener);
    
    adapters.add('adaptersEvents', function() {
      this.method = function() { return 'events method' };
    });
    
    assert.equal(willAdded, true);
    assert.equal(didAdded, true);
    assert.equal(notTriggered, true);
  });
});