var lodash = require('lodash');

/** EventEmitter **/
var EventEmitter = function() {
  
  // { name: [Function] }
  this.events = {};
};

// (name: String, listener: Function)
EventEmitter.prototype.on = function(name, listener) {
  var eventEmitter = this;
  
  if (typeof(listener) != 'function') {
    throw new Error('Listener must be a function.');
  }
  
  if (!eventEmitter.events[name]) {
    eventEmitter.events[name] = [];
  }
  
  eventEmitter.events[name].push(listener);
  
  return this;
};

// (name: String, listener: Function)
EventEmitter.prototype.off = function(name, listener) {
  if (typeof(listener) != 'function') {
    throw new Error('Listener must be a function.');
  }
  
  if (this.events[name]) {
    lodash.remove(this.events[name], function(handler) {
      return handler == listener;
    });
  }
};

// (name: String, args: Array) => Array
EventEmitter.prototype.emit = function(name, args) {
  var results = [];
  if (this.events[name]) {
    var result;
    for (var l in this.events[name]) {
      try {
        result = this.events[name][l].apply(this, args);
      } catch(error) {
        result = error;
      }
      if(result) {
        if (lodash.isArray(result)) {
          results.push.apply(results, result);
        } else {
          results.push(result);
        }
      }
    }
  }
  return results;
};

/** Adapter **/
// new (adapterName: String, adapters: Adapters, adapter: Function|Object)
var Adapter = function(adapterName, adapters, adapter) {
  this.adapters = adapters;
  
  // { name: [Function] }
  this.events = {};
  
  // { name: any }
  this.usedClasses = {};
  
  this.adapterName = adapterName;
  
  if (typeof(adapter) == 'function') {
    adapter.call(this, adapters);
  } else if(typeof(adapter) == 'object') {
    lodash.merge(this, adapter);
  } else {
    throw new Error('Invalid adapter "'+adapterName+'": '+adapter);
  }
  var results = adapters.emit('adapterWillAdded', [adapterName, this]);
  if (results.length) throw results[0];
};

Adapter.prototype = new EventEmitter();

// (name: String) => any
Adapter.prototype.asClass = function(name) {
  if (this.usedClasses[name]) {
    return this.usedClasses[name];
  } else {
    if (this.adapters.addedClasses[name]) {
      this.usedClasses[name] = this.adapters.addedClasses[name](this);
      return this.usedClasses[name];
    } else {
      throw new Error('Class "'+name+'" is not added into this adapters instance.');
    }
  }
};

/** Adapters **/
// new ()
var Adapters = function() {
  
  // { name: [Function] }
  this.events = {};
  
  // { adapterName: Adapter }
  this.adapters = {};
  
  // { name: Class }
  this.addedClasses = {};
};

// { name: (constructor: Function) }
Adapters.addedClass = Adapter.addedClass;

// (name: String, constructor: Function)
Adapters.addClass = function(name, constructor) {
  Adapter.addClass(name, constructor);
};

Adapters.prototype = new EventEmitter();

// (name: String, adapter: Function|Object)
Adapters.prototype.add = function(name, adapter) {
  if (this.adapters[name]) {
    throw new Error('Adapter "'+name+'" is already added.');
  }
  this.adapters[name] = new Adapter(name, this, adapter);
  this.emit('adapterDidAdded', [name, this.adapters[name]]);
};

// (name: String) => Adapter
Adapters.prototype.get = function(name) {
  return this.adapters[name];
};

// (name: String, constructor: Function)
Adapters.prototype.addClass = function(name, constructor) {
  if (this.addedClasses[name]) {
    throw new Error('Class "'+name+'" is already added in this adapters instance.');
  }
  this.addedClasses[name] = constructor;
};

exports.EventEmitter = EventEmitter;
exports.Adapter = Adapter;
exports.Adapters = Adapters;