# Adapters^0.0.3 ^[wiki](https://gitlab.com/ancient/adapters/wikis/home)

Intermediate interface between storage entities with the unique interface and universal application interfaces.

###### Example:

```js
/** Dependencies **/

var Adapters = require('ancient-adapters');

/** Construct adapters list **/

var adapters = new Adapters();

/** Listen additing of adapters **/

adapters.on('adapterWillAdded', function(adapterName, adapter) {
  console.log(adapterName, adapter.someAdapterLevelVariable, 'adapterWillAdded');
  // You can throw error for abort adding.
});

adapters.on('adapterDidAdded', function(adapterName, adapter) {
  console.log(adapterName, adapter.someAdapterLevelVariable, 'adapterDidAdded');
});

/** Defined some adapter **/

// As object.
var adapter = {
  // methods and variables of adapterName adapter
  someAdapterLevelMethod: function() {
    this.emit('adapterLevelEvent', [17]);
  },
  someAdapterLevelVariable: true,
};

// Or equal as function-constructor of object.

var adapter = function() {
  // methods and variables of adapterName adapter
  this.someAdapterLevelMethod = function() {
    this.emit('adapterLevelEvent', [17]);
  };
  this.someAdapterLevelVariable = true;
  return this;
};

/** Add defined adapters into adapters list **/

adapters.add('posts', adapter);
// posts true adapterWillAdded
// posts true adapterDidAdded
  
adapters.get('posts') == adapter; // true

var listener = function(someArgument) {
  console.log(someArgument, 'adapterLevelEvent');
};
adapters.get('posts').on('adapterLevelEvent', listener);

adapters.get('posts').someAdapterLevelMethod();
// 17 'adapterLevelEvent'

adapters.get('posts').off('adapterLevelEvent', listener);

adapters.get('posts').adapterName;
// 'posts'

/** Custom classes from adapter. **/

var SomeAdapterVariableGetter = function(adapter) {
  this.adapter;
};

SomeAdapterVariableGetter.prototype.get = function() { return this.adapter.someAdapterLevelVariable; };

adapters.addClass('SomeAdapterVariableGetter', function(adapter) {
  return new SomeAdapterVariableGetter(adapter);
});

var someAdapterVariableGetter = adapters.get('posts').asClass('SomeAdapterVariableGetter');
someAdapterLevelMethod.get(); // true
```

## Versions

### 0.0.3
* New syntax for custom classes.

### 0.0.2
* Add `adapterName` option into `Adapter` class instance.